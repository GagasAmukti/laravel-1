<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
</head>
<body>
    <h1>Buat Account Baru</h1>
    <h3>Sign Up Form</h3>
    <form method="POST" action="/welcome">
        @csrf
        <label for="fname">First Name :</label><br>
        <input type="text" id="fname" name="fname"><br>
        
        <label for="lname">Last Name :</label><br>
        <input type="text" id="lname" name="lname"><br>
    
        <p>Gender : </p>
        <input type="radio" id="Laki-Laki" name="laki" value="Laki-Laki">
        <label for="Laki-Laki">Laki-Laki</label><br>
        
        <input type="radio" id="perempuan" name="perempuan" value="Perempuan">
        <label for="perempuan">Perempuan</label><br>
        
        <input type="radio" id="others" name="others" value="others">
        <label for="others">Others</label>
      
        <p>Nationality : </p>
        <select name="Nationality" id="Nationality">
              <option value="Indonesia">Indonesian</option>
              <option value="Malaysia">Malaysia</option>
              <option value="Singapore">Singapore</option>
              <option value="Australia">Australia</option>
          </select>

        <p>Languange Spoken : </p>
        <input type="checkbox" id="bahasa1" name="bahasa1" value="Indonesia">
        <label for="bahasa1">Bahasa Indonesia</label><br>
        
        <input type="checkbox" id="bahasa2" name="bahasa2" value="English">
        <label for="bahasa2">English</label><br>
        
        <input type="checkbox" id="bahasa3" name="bahasa3" value="others">
        <label for="bahasa3">Others</label><br><br>

        <label for="biodata">Bio :</label><br>
        <textarea id="biodata" name="biodata" rows="10" cols="30">
        </textarea><br>
        <input type="submit" value="Sign Up">
    </form>
    
</body>
</html>